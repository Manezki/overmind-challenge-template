import sc2
from sc2.constants import UnitTypeId

class Macro():
    def __init__(self, mybot):
        self.mybot = mybot
        self.main_base = None



    def first_step(self):
        """
        
        """
        self.main_base = self.mybot.units(UnitTypeId.COMMANDCENTER)[0]


    async def action(self, iteration):
        
        actions = []

        if iteration == 0:
            self.first_step()

        for scv in self.mybot.units(UnitTypeId.SCV).idle:
            actions.append(scv.gather())


        if self.mybot.supply_left < 3:
            if self.mybot.can_afford(UnitTypeId.SUPPLYDEPOT) and self.mybot.already_pending(UnitTypeId.SUPPLYDEPOT) < 2:
                await self.mybot.build(UnitTypeId.SUPPLYDEPOT,
                                       near=self.main_base.position.towards(self.mybot.game_info.map_center, 8))


        for cc in self.mybot.units(UnitTypeId.COMMANDCENTER).ready.noqueue:
            if self.mybot.can_afford(UnitTypeId.SCV) and cc.assigned_harvesters < cc.ideal_harvesters:
                actions.append(cc.train(UnitTypeId.SCV))
        await self.mybot.do_actions(actions)